import {combineReducers} from 'redux';
import ApplicationReducer from './application';

export default combineReducers({
    // Add here for combine
    application: ApplicationReducer,
});