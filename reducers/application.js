import * as actionTypes from '@actions/actionTypes';

const initialState = {
  language: null,
  // define here for future initialState 
};

export default (state = initialState, action = {}) => {
  // use switch case (prepare for future state)
  switch (action.type) {
    case actionTypes.CHANGE_LANGUAGE:
      return {
        ...state,
        language: action.language,
      };
    default:
      return state;
  }
};
