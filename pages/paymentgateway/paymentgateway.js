import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Pressable, Linking } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LottieView from 'lottie-react-native'

const remainingTexts = {
  23000: "Granite: Rp.10000, Marble: Rp.8000, Insurance: Rp.5000",
  18000: "Granite: Rp.10000, Marble: Rp.8000",
  15000: "Granite: Rp.10000, Marble: Rp.5000",
  13000: "Marble: Rp.8000, Insurance: Rp.5000",
  10000: "Granite: Rp.10000",
  8000: "Marble: Rp.8000",
  5000: "Insurance: Rp.5000",
};

const getRemainingTexts = (remaining) => {
  if (remainingTexts.hasOwnProperty(remaining)) {
    return remainingTexts[remaining].split(',').map((item, index) => <Text key={index} style={{color: '#fff'}}>{item}</Text>);
  } else {
    return [<Text key={0} style={{color: '#fff'}}>Remaining: {remaining}</Text>];
  }
};

const RadioButton = () => {

  const [grossAmount, setGrossAmount] = useState(null);
  const [address, setAddress] = useState(null)
  const [service, setService] = useState(null)

  useEffect(() => {
    const fetchGrossAmount = async () => {
      const storedGrossAmount = await AsyncStorage.getItem('gross_amount');
      setGrossAmount(Number(storedGrossAmount));
    };
    
    fetchGrossAmount();
  }, []);
  
  useEffect(() => {
    const fetchAddress = async () => {
      const storedAddress = await AsyncStorage.getItem('address')
      setAddress(String(storedAddress));
      console.log("ini address",storedAddress)
    }
  
    fetchAddress();
  }, []);

  useEffect(() => {
    const fecthService = async () => {
      const storedService = await AsyncStorage.getItem('service')
      setService(String(storedService))
    }

    fecthService();
  }, [])

  const [url, setUrl] = useState('')

//   AsyncStorage.getAllKeys((err, keys) => {
//     AsyncStorage.multiGet(keys, (error, stores) => {
//       stores.map((result, i, store) => {
//         console.log({ [store[i][0]]: store[i][1] });
//         return true;
//       });
//     });
//   });
  AsyncStorage.getItem('url').then((res) => setUrl(res))
  console.log(url)

  const options = [100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 500000, 1000000];
  const closestPrice = options.reduce((prev, curr) => {
    return curr <= grossAmount ? curr : prev;
  }, options[0]);

  const remaining = grossAmount - closestPrice - 7500
  const remainingTexts = getRemainingTexts(remaining);

  return (
    <View style={styles.container}>
      <LottieView 
        source={require('../../assets/animation/bill.json')}
        autoPlay
        loop
        style={{
            height: hp('45%'),
            marginBottom: hp('15%')
        }}/>
      <View style={{marginTop: hp('-10%'),}}></View>
      <View style={styles.shapebill2}>
        <View style={styles.shapebill}>
        <View style={{justifyContent: 'center', alignItems: 'center', textAlign: 'center'}}>
          <Text style={styles.pricetext}>Detail Pembayaran</Text>
          <View style={{flexDirection: 'row', marginTop: '2.5%', justifyContent: 'flex-start', alignItems: 'flex-start'}}>
          <Text style={{color: '#fff', marginRight: '3%'}}>Jenis Layanan:</Text>
          <Text style={{color: '#fff', width: '40%'}}>{service} Cleaning</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: '1%', marginBottom: '0.6%', marginLeft: '-1.3%'}}>
          <Text style={{color: '#fff', marginRight: '3%'}}>Alamat:</Text>
          <Text style={{color: '#fff', width: '50%', marginHorizontal: 'auto'}}>{address?address.substring(0, 45):'tunggu =ya'}...</Text>
          </View>
          <View>
            <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', textAlign: 'left', marginTop: '1%'}}>
                <Text style={{color: '#fff', marginRight: '3%'}}>Harga Pokok:</Text>
                <Text style={{color: '#fff'}}>Rp.{closestPrice}</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', textAlign: 'left', marginTop: '2%'}}>
                <Text style={{color: '#fff', marginRight: '3%'}}>Platform Fee:</Text>
                <Text style={{color: '#fff'}}>Rp.7500</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', textAlign: 'left', marginTop: '2%'}}>
                <Text style={{color: '#fff', marginRight: '3%'}}>Add-ons:</Text>
                <Text style={{color: '#fff', width: '50%'}}>{remainingTexts}</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', textAlign: 'center', marginTop: '2%'}}>
                <Text style={{color: '#fff', marginRight: '3%'}}>Diskon:</Text>
                <Text style={{color: '#fff'}}>-</Text>
              </View>
            </View>
          </View>
          </View>
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    width: wp('100%'),
    height: hp('100%')
  },
  shapebill:{
    backgroundColor: '#DA7DE1',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderRadius: 20,
    paddingBottom: hp('6.5%'),
    marginTop: '-3%',
    marginBottom: '5%'
  },
  shapebill2:{
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  pricetext: {
    fontFamily: 'Ubuntu',
    fontSize: 28,
    width: 300,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: '#fff',
    marginTop: hp('2%')
  },
  descp:{
    width: wp('70%'),
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginTop: hp('0.5%'),
    color: '#fff'
  },
nextbutton: {
    backgroundColor: '#DA7DE1',
    height: 45,
    borderRadius:30,
    width: '40%',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginTop: 20,
    color: '#fff',
    fontFamily: 'Ubuntu',
    marginRight: '2.5%',
    marginBottom: '5.4%'
},
nextbutton2: {
  backgroundColor: '#353535',
  height: 45,
  borderRadius: 1130,
  width: '40%',
  alignItems: 'center',
  justifyContent: 'center',
  textAlign: 'center',
  color: '#fff',
  fontFamily: 'Ubuntu',
},
textbutton2: {
    fontFamily: 'Ubuntu',
    color: '#fff',
    fontSize: 12
},
textbutton: {
  fontFamily: 'Ubuntu',
  color: '#fff',
},
});

export default function App({navigation}) {

  const [url, setUrl] = useState('');

  AsyncStorage.getItem('url').then((res) => setUrl(res));
  console.log(url);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff'}}>
      <RadioButton/>  
      <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: '-5%', marginBottom: '5%', marginRight: '0.5%'}}>
        <Pressable style={styles.nextbutton} onPress={() => Linking.openURL(`${url}`)}>
          <Text style={styles.textbutton}>BAYAR DISINI</Text>
        </Pressable>
        <Pressable style={styles.nextbutton2} onPress={() => navigation.navigate('PaymentSuccessful')}>
          <Text style={styles.textbutton2}>SAYA SUDAH BAYAR</Text>
        </Pressable> 
        </View>
    </View>
  );
}