import { StyleSheet, View, Text, Image, ScrollView, TouchableOpacity, Button } from 'react-native'
import React, { useEffect, useState} from 'react'
import { Gap } from '../../components'
import { Stars, Pencil } from '../../assets'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import * as ImagePicker from 'expo-image-picker';
import * as SecureStore from 'expo-secure-store';
import {useTranslation} from 'react-i18next';


const Profile = ({navigation}) => {
  const {t} = useTranslation();
  const [profilePhoto, setProfilePhoto] = useState(null);

  useEffect(() => {
    loadProfilePhoto();
  }, []);

  const loadProfilePhoto = async () => {
    try {
      const photoUri = await SecureStore.getItemAsync('profilePhotoUri');
      if (photoUri) {
        setProfilePhoto(photoUri);
      }
    } catch (error) {
      console.log('Error loading photo URI:', error);
    }
  };

  const handlePhotoUpload = async () => {
    const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (!permissionResult.granted) {
      alert('Permission to access camera roll is required!');
      return;
    }

    const pickerResult = await ImagePicker.launchImageLibraryAsync();
    if (pickerResult.cancelled) {
      return;
    }

    const photoUri = pickerResult.uri;
    setProfilePhoto(photoUri);

    try {
      await SecureStore.setItemAsync('profilePhotoUri', photoUri);
    } catch (error) {
      console.log('Error saving photo URI:', error);
    }
  };

  const [data, setMemberData] = useState('');

  useEffect(() => {

    const fetchMemberData = async () => {
      try {
        const id = await AsyncStorage.getItem('id')
        const link = `https://customer.kilapin.com/users/${id}`
        const response = await fetch(link);
        const data = await response.json()
        console.log("response",data.data)
        setMemberData(data.data)
        
  
      } catch (error) {
        
      }  
    }
    fetchMemberData()
  },[])

  const [soundEnabled, setSoundEnabled] = useState(true);

  const handleSoundToggle = () => {
    setSoundEnabled(!soundEnabled);
  };

  let textToRender = '';

  if (data.point > 5000) {
    textToRender = 'Si Paranoid';
  } else if (data.point > 750) {
    textToRender = 'Si Perfect';
  } else if (data.point > 150) {
    textToRender = 'Si Suci';
  } else if (data.point < 150) {
    textToRender = 'Si Bersih';
  }


  return (
    <View style={styles.allcontainer}>
    <View style={styles.container}>
        <Text style={{marginTop: hp('12%'), fontFamily: 'Ubuntu', fontSize: 20, marginBottom: hp('4%')}}>Pengaturan Profil</Text>
        <View style={styles.topcleanerphoto}>
                  {profilePhoto ? (
        <Image source={{ uri: profilePhoto }}  style={{
          width: '100%',
          height: '100%',
          borderRadius: 100,}} />
      ) : (
        <Image
          source={require('../../assets/image/ProfileBroom.png')}
          style={{
            flex: 1,
            width: undefined,
            height: undefined,
            borderRadius: 100,}}/>
      )}
      <TouchableOpacity onPress={handlePhotoUpload} style={{marginLeft: '45%', marginTop: '-25%', backgroundColor: '#5865F2', alignItems: 'center', justifyContent: 'center', width: '35%', height: '35%', borderRadius: 200}} >
            <Pencil onPress={handlePhotoUpload}/>
      </TouchableOpacity>
            </View>
            <Gap height={20}/>
        <Text style={styles.profilename}>
          {(data.name)}
        </Text>
        <Gap height={5}/>
        <Text style={styles.phonenumber}>
          +62{(data.phone)}
          </Text>
        <Gap height={25}/>
        <View style={styles.benefit}>
          <View style={styles.pointback} onPress={() => navigation.navigate("XPPage")}>
            <Stars onPress={() => navigation.navigate("XPPage")} />
        <Text style={styles.point} onPress={() => navigation.navigate("XPPage")}>
          {(data.point)}  {textToRender}
          </Text> 
          </View>
        </View>
        <Gap height={25}/>
        <View height = '60%' flexGrow = {1} style={{ marginBottom: wp("6%"),}}>
        <ScrollView>
        <TouchableOpacity style={styles.optionContainer} onPress={() => navigation.replace('TermsAndCondition')}>
            <Icon name="document-text-outline" size={wp('6%')} color="#333" style={styles.optionIcon} />
            <Text style={styles.optionText}>{t('terms')}</Text>
            <Icon name="chevron-forward-outline" size={wp('6%')} color="#333" style={styles.optionArrowIcon} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.optionContainer}  onPress={() => navigation.navigate('FAQ')}>
            <Icon name="help-circle-outline" size={wp('7%')} color="#333" style={[styles.optionIcon, {marginLeft: '-0.5%' }]} />
            <Text style={styles.optionText}>{t('faq')}</Text>
            <Icon name="chevron-forward-outline" size={wp('6%')} color="#333" style={styles.optionArrowIcon2} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.optionContainer}  onPress={() => navigation.navigate('ChatBot')}>
          <Icon name="chatbubbles-outline" size={wp('6%')} color="#333" style={styles.optionIcon} />
            <Text style={styles.optionText}>{t('customer_assistance_center')}</Text>
            <Icon name="chevron-forward-outline" size={wp('6%')} color="#333" style={styles.optionArrowIcon5} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.optionContainer} onPress={() => navigation.navigate('ContactUs')}>
            <Icon name="call-outline" size={wp('6%')} color="#333" style={styles.optionIcon} />
            <Text style={styles.optionText}>{t('contact_us')}</Text>
            <Icon name="chevron-forward-outline" size={wp('6%')} color="#333" style={styles.optionArrowIcon3} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.optionContainer}  onPress={() => navigation.navigate('LanguageSelection')}>
            <Icon name="language-outline" size={wp('6%')} color="#333" style={styles.optionIcon} />
            <Text style={styles.optionText}>{t('language')}</Text>
            <Icon name="chevron-forward-outline" size={wp('6%')} color="#333" style={styles.optionArrowIcon4} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.optionContainer}  onPress={() => navigation.navigate('ManageAccount')}>
            <Icon name="person" size={wp('6%')} color="#333" style={styles.optionIcon} />
            <Text style={styles.optionText}>{t('profile_setting')}</Text>
            <Icon name="chevron-forward-outline" size={wp('6%')} color="#333" style={styles.optionArrowIcon6} />
          </TouchableOpacity>
      <View style={{padding: '30%'}}></View>
        </ScrollView>
        </View>
    </View>
    </View>
  )
}

const styles = StyleSheet.create({
  topcleanerphoto: {
    height: 120,
    width: 120,
    justifyContent: 'center',
    alignItems: 'center'
  },
  optionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: wp('5%'),
    paddingVertical: wp('4%'),
    backgroundColor: '#fff',
  },
  optionIcon: {
    marginRight: wp('13%'),
  },
  optionText: {
    fontSize: wp('4.5%'),
    color: '#000',
  },
  optionArrowIcon: {
    marginLeft: wp('22%'),
    justifyContent: 'flex-end'
  },
  optionArrowIcon2: {
    marginLeft: wp('56%'),
    justifyContent: 'flex-end'
  },
  optionArrowIcon3: {
    marginLeft: wp('36%'),
    justifyContent: 'flex-end'
  },
  optionArrowIcon4: {
    marginLeft: wp('49%'),
    justifyContent: 'flex-end'
  },
  optionArrowIcon5: {
    marginLeft: wp('35%'),
    justifyContent: 'flex-end'
  },
  optionArrowIcon6: {
    marginLeft: wp('40%'),
    justifyContent: 'flex-end'
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  allcontainer: {
    backgroundColor: '#fff'
  },
  benefit: {
    flexDirection: 'row',
  },
  point: {
    fontSize: 16,
    fontFamily: 'Ubuntu',
    color: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginLeft: 10,
  },
  pointback: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: '#FFB400',
    width: '50%',
    height: 40,
    borderRadius: 15,
    flexDirection: 'row',
  },
  profilename: {
    fontFamily: 'Ubuntu',
    fontSize: 24,
    color: '#1E2022',
  },
  phonenumber: {
    fontFamily: 'Ubuntum',
    fontSize: 16,
    color: '#77838F'
  },
})

export default Profile