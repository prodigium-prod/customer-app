import {
  StyleSheet,
  View,
  Text,
  ScrollView,
} from "react-native";
import React, { useEffect,useState } from "react";
import { Gap } from "../../components";
import {
  ProfileIcon,
  TimeIcon,
  DateIcon,
  LocationIcon,
  ProfileIconGreen,
  ProfileIconRed,
  DateIconGreen,
  DateIconRed,
  LocationIconGreen,
  LocationIconRed,
  TimeIconGreen, 
  TimeIconRed
} from "../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import AsyncStorage from '@react-native-async-storage/async-storage';

const Task = ({ navigation }) => {
  const [count, setCount] = useState(0);
  const [OrderHistory, setOrderHistory] = useState('');
  const [HistoryDone, setHistoryDone] = useState('');
  const [HistoryOtherDone, setHistoryOtherDone] = useState('');

  useEffect(() => {

    const fetchOrderStatus = async () => {
      try {
          var id = await AsyncStorage.getItem('id')
          const link = `https://customer.kilapin.com/order/history/${id}`
          const response = await fetch(link);
          const data = await response.json()
          console.log(data.data)
          setOrderHistory(data.data)
          setHistoryDone(OrderHistory.filter (item => item.status === "Done" || item.status === "Cancel" || item.status === "Waiting for Payment"))
          setHistoryOtherDone(OrderHistory.filter (item => item.status !== "Done" && item.status !== "Waiting for Payment" && item.status !== "Cancel"))
          await HistoryDone.sort((a,b) => new Date(b.createdAt) - new Date(a.createdAt))
          await HistoryOtherDone.sort((a,b) => new Date(b.createdAt) - new Date(a.createdAt))
      } catch (error) {
      }  
    }
    fetchOrderStatus()
    const intervalId = setInterval(() => {
      setCount(count => count + 1);
    }, 3000);
  return () => clearInterval(intervalId);
  },[count])

  const capitalizeWords = (input) => {
    return input.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
  };  

  return (
    <View style={styles.allcontainer}>
      <View style={styles.container}>
        <Text style={styles.order}>Order</Text>
        <Gap height={25} />
        <View style={styles.filtersec}>
          <Text style={styles.filtertext}>Your Order List</Text>
        </View>
        <Gap height={10} />
        <View height = '80%' flexGrow = {1}>
          <ScrollView contentContainerStyle={{
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",}}>
            <View style={styles.orderhistoryscroll}>
            <View>
            {(
              HistoryOtherDone
              ) ? (<View>
              {HistoryOtherDone.map((OrderHistory)=> {
              return (
                <View style={[styles.ordercard,     {borderColor: 
                  OrderHistory.order_status === "Done" ? "#32CD32" :
                  OrderHistory.order_status === "Canceled" ? "#FF6347" : "#54CC76"
                }]}>
                    <View>
                    <View
                  style={styles.topcontentcard}
                  onPress={() => navigation.navigate("Tracking")}
                  key = {OrderHistory.order_id}
                >
                  <View>
                    <Text style={styles.ordertitle1}>{OrderHistory.service} Cleaning</Text>
                    <Text style={styles.ordertitle2}>
                      {OrderHistory.order_id}
                      {/* {capitalizeWords(OrderHistory.item_name.replace(/-/g, ' ').split(' ').slice(0, 16).join(' '))}... */}
                    </Text>
                  </View>
                  <View
                  style={[styles.orderstatus, {backgroundColor: 
                    OrderHistory.order_status === "Done" ? "#32CD32" :
                    OrderHistory.order_status === "Canceled" ? "#FF6347" : "#54CC76"
                  }]}
                    onPress={() => navigation.navigate("Tracking", {order_id: OrderHistory.order_id})}
                  >
                    <Text
                      style={styles.orderstatustext}
                      disabled={OrderHistory.order_status==="Waiting for Payment" || OrderHistory.order_status==="Done" || OrderHistory.order_status==="Open" || OrderHistory.order_status==="Ready to Book"}
                      onPress={() => navigation.navigate(
                        "Tracking", {order_id: OrderHistory.order_id}
                        )}
                      
                    >
                      {(OrderHistory.order_status === ("Open") || OrderHistory.order_status === ("Ready to Book")) ? ("Waiting For Cleaner") : ((OrderHistory.order_status === "Waiting for Payment")?("Canceled"):(OrderHistory.order_status))}
                    </Text>
                  </View>
                </View>
                <Gap height={20} />
                <View style={styles.person}>
                {OrderHistory.order_status === "Done" ? <ProfileIcon /> : <ProfileIconGreen />}
                  <Gap width={10} />
                  <Text style={styles.persontext}>{OrderHistory.cleaner_id ? ("Cleaner Ranger"):("Wait!")}</Text>
                  <Gap width={35} />
                  <View style={styles.date2}>
                  {OrderHistory.order_status === "Done" ? <DateIcon /> : <DateIconGreen />}
                    <Gap width={10} />
                    <Text style={styles.persontext}>
                      {/* waktu */}
                      {OrderHistory.createdAt}
                      </Text>
                  </View>
                </View>
                <Gap height={20} />
                <View style={styles.person}>
                {OrderHistory.order_status === "Done" ? <TimeIcon /> : <TimeIconGreen />}
                  <Gap width={10} />
                  <Text style={styles.persontext}>
                    {OrderHistory.service === "Urgent" ? ("Now") : 
                    (OrderHistory.time.slice(0,5))
                    // 'time'
                    }
                  </Text>
                  <View style={styles.date}>
                    <Gap width={20} />
                    {OrderHistory.order_status === "Done" ? <LocationIcon /> : <LocationIconGreen />}
                    <Gap width={10} />
                    <Text style={{fontFamily: 'Ubuntur', }}>
                      {OrderHistory.address}
                      {/* {OrderHistory.address.split(' ').slice(0,4).join(' ')}... */}
                      </Text>
                  </View>
                </View>
                </View>
              </View>
              )
            })}
            </View>) : (<Text>Loading...</Text>)}
            {(

              HistoryDone
              ) ? (<View>
              {HistoryDone.map((OrderHistory)=> {
              return (
                <View style={[styles.ordercard,     {borderColor: 
                  OrderHistory.status === "Done" ? "#DA7DE1" :
                  OrderHistory.status === "Canceled" ? "#FF6347" : "#CC5353"
                }]}>
                    <View>
                    <View
                  style={styles.topcontentcard}
                  onPress={() => navigation.navigate(
                    "Tracking", {order_id: OrderHistory.order_id})}
                  key = {OrderHistory.order_id}
                >
                  <View>
                    <Text style={styles.ordertitle1}>{OrderHistory.service} Cleaning</Text>
                    <Text style={styles.ordertitle2}>{OrderHistory.item_name}</Text>
                  </View>
                  <View
                    style={[styles.orderstatus, {backgroundColor: 
                      OrderHistory.status === "Done" ? "#DA7DE1" :
                      OrderHistory.status === "Canceled" ? "#FF6347" : "#CC5353"
                    }]}
                    onPress={() => navigation.navigate("Tracking", {order_id: OrderHistory.order_id})}
                  >
                    <Text
                      style={styles.orderstatustext}
                      disabled={OrderHistory.status==="Waiting for Payment" || OrderHistory.status==="Done" || OrderHistory.status==="Open" || OrderHistory.status==="Ready to Book"}
                      onPress={() => navigation.navigate(
                        "Tracking", {order_id: OrderHistory.order_id}
                        )}>
                      {(OrderHistory.status === ("Open") || OrderHistory.status === ("Ready to Book")) ? ("Waiting For Cleaner") : ((OrderHistory.status === "Waiting for Payment")?("Canceled"):(OrderHistory.order_status))}
                    </Text>
                  </View>
                </View>
                <Gap height={20} />
                <View style={styles.person}>
                {OrderHistory.order_status === "Done" ? <ProfileIcon /> : <ProfileIconRed />}
                  <Gap width={10} />
                  <Text style={styles.persontext}>{OrderHistory.cleaner_id ? ("Cleaner Ranger"):("Wait!")}</Text>
                  <Gap width={35} />
                  <View style={styles.date2}>
                  {OrderHistory.status === "Done" ? <DateIcon /> : <DateIconRed />}
                    <Gap width={10} />
                    {/* <Text style={styles.persontext}>{OrderHistory.created_at.slice(0,10)}</Text> */}
                  </View>
                </View>
                <Gap height={20} />
                <View style={styles.person}>
                 {OrderHistory.status === "Done" ? <TimeIcon /> : <TimeIconRed />}
                  <Gap width={10} />
                  <Text style={styles.persontext}>
                    {OrderHistory.service === "Urgent" ? ("Now") : 
                    'service'
                    // (OrderHistory.time.slice(0,5))
                    }
                  </Text>
                  <View style={styles.date}>
                    <Gap width={20} />
                    {OrderHistory.status === "Done" ? <LocationIcon /> : <LocationIconRed />}
                    <Gap width={10} />
                    <Text style={{fontFamily: 'Ubuntur', }}>
                      isinya
                      {/* {OrderHistory.address.split(' ').slice(0,4).join(' ')}... */}
                      </Text>
                  </View>
                </View>
                </View>
              </View>
              )
            })}
            </View>) : (<Text>Loading...</Text>)}
          </View>

            </View>
          </ScrollView>
        </View>
        <View style={{backgroundColor: '#fff', padding: '2%'}}></View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#fff'
  },
  filtersec: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  filtertext: {
    fontFamily: "Ubuntum",
    fontSize: 14,
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    marginBottom: 15,
  },
  order: {
    fontFamily: "Ubuntu",
    fontSize: 20,
    color: "#1E2022",
    marginTop: hp('4%')
  },
  ordercard: {
    width: 320,
    height: 140,
    backgroundColor: "#FFF",
    borderColor: '#DA7DE1',
    borderWidth: 2,
    borderRadius: 15,
    marginBottom: hp('3%'),
  },
  topcontentcard: {
    alignItems: "center",
    justifyContent: "space-between",
    textAlign: "center",
    flexDirection: "row",
  },
  orderstatus: {
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    backgroundColor: "#DA7DE1",
    borderRadius: 20,
    width: 95,
    height: 30,
    marginRight: 12,
    marginTop: 10,
    fontSize: 12,
    marginLeft: '-3%'
  },
  orderstatustext: {
    color: "#fff",
    fontFamily: "Ubuntu",
    fontSize: 12,
  },
  ordertitle1: {
    marginTop: 12,
    marginLeft: 12,
    color: "#1E2022",
    fontSize: 12,
    fontFamily: "Ubuntu",
    marginBottom: 2
  },
  ordertitle2: {
    marginLeft: 12,
    fontSize: 10,
    fontFamily: "Ubuntur",
    width: '90%',
    marginBottom: -2
  },
  person: {
    marginLeft: 12,
    flexDirection: "row",
  },
  persontext: {
    fontFamily: "Ubuntur",
    color: "#4B4B4B",
  },
  date: {
    flexDirection: "row",
  },
  date2: {
    flexDirection: "row",
    marginLeft: '-5%'
  },
}
);

export default Task;
