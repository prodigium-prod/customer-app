import { Gap } from '../../components';
import { StyleSheet, View, Text, Pressable, TextInput, BackHandler, Alert } from 'react-native'
import React, { useEffect, useState } from 'react'
import { LinearGradient } from 'expo-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LottieView from 'lottie-react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { auth } from "../../config/firebase";
import { signInWithEmailAndPassword } from "firebase/auth";

const Login = ({ navigation }) => {
  useEffect(() => {
    const backAction = () => {
      navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);

    return () => backHandler.remove();
  }, [navigation]);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleLogin = async () => {
    try {
      const response = await fetch('https://customer.kilapin.com/users/login-mongose', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password,
        }),
      });
  
      const data = await response.json();
      console.log('Response data:', data);
  
      if (response.ok && data.data) {
        await AsyncStorage.setItem('id', data.data._id);
  
        signInWithEmailAndPassword(auth, 'revan1@gmail.com', 'qwerty123')
        .then(() => {
          console.log("Login success", auth);
          AsyncStorage.setItem('loggedIn', 'true');
          navigation.reset({
            index: 0,
            routes: [{ name: 'MainApp' }],
          });
        })
        .catch((err) => Alert.alert("Login error", err.message));
      
      } else {
        setErrorMessage(data.message);
      }
    } catch (error) {
      console.error(error);
    }
  };
  

  const [opacity, setOpacity] = useState(1);

  const handlePressIn = () => {
    setOpacity(0.5);
  };

  const handlePressOut = () => {
    setOpacity(1);
  };

  useEffect(() => {
    const checkLoggedInStatus = async () => {
      try {
        const value = await AsyncStorage.getItem('loggedIn');
        if (value === 'true') {
          navigation.reset({
            index: 0,
            routes: [{ name: 'MainApp' }],
          });
        }
      } catch (error) {
        console.error(error);
      }
    };
  
    checkLoggedInStatus();
  }, [navigation]);
  
  

  return (
    <View style={styles.container}>
      <LinearGradient colors={['#fff', '#fff']} style={styles.backgroundgradient}>
          <LottieView 
        source={require('../../assets/animation/kilapin.json')}
        autoPlay
        loop
        style={styles.namelogo}/>
          <View style={styles.shape}>
          <Gap height={30} />
            {errorMessage ? <Text style={styles.error}>{errorMessage}</Text> : null}
            <Gap height={16} />
      <TextInput style={styles.input} placeholder="Email" onChangeText={setEmail} value={email} autoCapitalize="none"/>
            <Gap height={16} />
      <TextInput style={styles.input} placeholder="Password" onChangeText={setPassword} value={password} secureTextEntry={true} />
            <Text style={styles.maintext}>Lupa password</Text>
            <Pressable
      style={({ pressed }) => [
        styles.press,
        {
          opacity: pressed ? 0.5 : opacity,
        },
      ]}
      onPress={handleLogin}
      onPressIn={handlePressIn}
      onPressOut={handlePressOut}
    >
              <Text style={styles.buttontext} 
              onPress={handleLogin} 
              onPressIn={handlePressIn}
              onPressOut={handlePressOut}
              >SIGN IN</Text>
          </Pressable>
            <Text style={styles.accsignup}>Tidak punya akun? <Text style={styles.daftartext} 
            onPress={() => navigation.navigate('SignUp')}
            >Daftar</Text>
            </Text>
            <Gap height={20} />
          </View>
      </LinearGradient>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex:1 ,
  },
  backgroundgradient: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
  },
  namelogo: {
     flex: 1,
      color:'#fff',
      fontFamily: 'Ubuntu',
      fontSize: 32,
      height: hp('25%'),
      marginTop: hp('2%'),
      marginBottom: hp('-5%'),
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
  },
  shape: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: '#fff',
    alignSelf: 'stretch',
    textAlign: 'center',
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    marginBottom: ('15%'),
    paddingTop: hp('8%')
  },
  maintext: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: '#5865F2',
    fontFamily: 'Ubuntur',
    fontSize: 16,
    marginTop: 20,
    width: 300,
    marginBottom: -5,
  },
  input:{
    borderWidth: 1.5,
    borderColor: '#8D8D8D',
    height: 51,
    borderRadius:30,
    width: 300,
    padding: 15,
    fontFamily: 'Ubuntur',
  },
  press: {
    justifyContent: 'center',
    marginTop: 15,
    backgroundColor: '#DA7DE1',
    height: 51,
    borderRadius:30,
    width: 300,
  },
  buttontext:{
    color: '#fff',    
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontFamily: 'Ubuntu',
    fontSize: 16,
  },
  accsignup: {
    marginTop: 20,
  },
  daftartext: {
    color: '#DA7DE1',
    flexDirection: 'column',
    justifyContent: 'center', 
    alignContent: 'center', 
    alignItems: 'center',
  },
})
export default Login