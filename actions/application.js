import * as actionTypes from './actionTypes';

const changeLanguage = language => {
    return {
        type: actionTypes.CHANGE_LANGUAGE,
        language,
    };
};

export const onChangeLanguage = language => dispatch => {
    dispatch(changeLanguage(language));
  };