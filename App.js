import 'react-native-gesture-handler'
import React from 'react';
import {store, persistor} from './store';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import { NavigationContainer } from '@react-navigation/native';
import Router from './router'
import { StyleSheet } from 'react-native';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer >
          <Router/>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}

export default App